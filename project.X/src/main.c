#define _XTAL_FREQ 20000000

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <xc.h>

#include <libpic30.h>
#include <p33FJ128MC804.h>

#include "init.h"

// FOSCSEL
#pragma config FNOSC = PRIPLL
// FOSC
#pragma config POSCMD = XT
#pragma config OSCIOFNC = OFF
#pragma config IOL1WAY = ON
#pragma config FCKSM = CSDCMD
// FWDT
#pragma config FWDTEN = OFF
// FICD
#pragma config ICS = PGD3
#pragma config JTAGEN = OFF


int main(int argc, char** argv)
{
    initDSP();

    LATAbits.LATA4 = 1;
    return (EXIT_SUCCESS);
}
