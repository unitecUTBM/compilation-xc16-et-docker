#include "init.h"

#include <dsp.h>
#include <xc.h>
#include <libpic30.h>
#include <p33FJ128MC804.h>

void initDSP() {
    RCONbits.SWDTEN = 0;

    PLLFBD = PLLREG_PLLFBD;
    CLKDIVbits.PLLPOST = PLLREG_PLLPOST;
    CLKDIVbits.PLLPRE = PLLREG_PLLPRE;

    __builtin_write_OSCCONH(0x03);
    __builtin_write_OSCCONL(0x01);

    while (OSCCONbits.COSC != 0b011);
    while (OSCCONbits.LOCK != 1);
}
