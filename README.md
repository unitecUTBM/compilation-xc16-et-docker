# Compilation de projet MPLABX avec XC16, docker et Protocol Buffer

Un Docker qui contient:
* compilateur XC16 avec la bibliothèque des périphériques
* make
* Nanopb

## XC16

XC16 est le compilateur de Microchip pour la compilation vers les microcontrôleurs PIC24 et dsPIC33. Ce compilateur supporte le langage C et l'assembleur. Le compilateur est généralement utilisé avec une bibliothèque de périphérique qui va permettre de manipuler les registres bit à bit sans avoir besoin de chercher leurs adresses dans la documentation.

[Documentation officielle](https://www.microchip.com/stellent/groups/techpub_sg/documents/devicedoc/en559023.pdf)

### Compilation

#### En une ligne de commande

Pour compiler un programme, on utilise la commande `xc16-gcc`. Cette commande à besoin de plusieurs choses pour pouvoir fonctionner. Elle a besoin de savoir :
* quels fichiers compiler
* quel nom donner au programme compilé
* la référence du microcontrôleur cible qui exécutera le code
* les flags
* le script du linker

Construisons une commande pour compiler les fichiers `main.c` et `init.c`. On définit le nom du fichier qui contiendra le programme compilé avec l'argument '-o' (Nous l'appelleront build.elf)
```bash
xc16-gcc main.c init.c -o build.elf
```

On doit ensuite spécifier le microcontrôleur utilisé et les flags. Pour le microcontrôleur, on utilise l'argument `-mcpu=` suivi par sa référence. Pour notre exemple, on va compiler pour un dsPIC33FJ128MC804, mais on ne doit pas préciser `dsPIC` pour cette famille de composant, on doit donc ajouter `-mcpu=33FJ128MC804`.

Les flags sont des options de compilation. On utilise généralement les flags `-Wall` et `-Werror`. `-Werror` transforme les warning en erreur, et `-Wall` rend le compilateur moins permissif. Ces flags de sont pas obligatoire, mais c'est une bonne pratique pour s'obliger à avoir un code propre. On peut aussi utiliser l'option `-I.` pour pouvoir ajouter des répertoires en temps que bibliothèque (Dans ce cas `.` soit le répertoire courant). L'argument `-omf=elf` sert à définir le type de fichier à générer. On obtient donc :

```bash
xc16-gcc main.c init.c -Wall -Werror -I. -mcpu=33FJ128MC804 -omf=elf -o build.elf
```

On termine par ajouter un script du linker adapté pour le microcontrôleur. Ce script définit notamment la taille du heap et de la pile, ainsi c'est la définition du vecteur d'interruption, on voit notamment les arguments `--heap=512`, `--stack=512` et `--script=p33FJ128MC804.gld` qui définissent les tailles et le script du linker. Les autres options sont des paramètres plus ou moins obscurs générés par MPLAB X, l'IDE de Microchip.

```bash
xc16-gcc main.c init.c -Wall -Werror -I. -mcpu=33FJ128MC804 -omf=elf -o build.elf -Wl,--defsym=__MPLAB_BUILD=1,--script=p33FJ128MC804.gld,--heap=512,--stack=512,--check-sections,--data-init,--pack-data,--handles,--isr,--no-gc-sections,--fill-upper=0,--stackguard=16,--no-force-link,--smart-io
```

> Mais attendez ! Pourquoi vous fuyez ? Vous allez voir, c'est pas si difficile !

##### Heap

Le tas (ou Heap) est une zone de la mémoire qui est réservé pour les allocations dynamiques. Cet espace est donc utilisé au cas où le programme demande temporairement à utiliser de la mémoire.

##### Stack

La pile (ou Stack) est une zone de la mémoire dédiée à stocker les fonctions qui sont en cours d'utilisation. Chaque fois qu'une fonction est appelée, le contexte est ajouté dans cette zone. Donc quand on a besoin d'appeler plusieurs fonctions à l'intérieur d'autres fonctions, la mémoire forme une pile pour stocker toutes les informations.

##### Vecteur d'interruption

Un microcontrôleur peut lancer des fonctions quand il détecte un évènement. Par exemple, on peut programmer le microcontrôleur pour générer une interruption quand un timer dépasse une certaine valeur pour créer des temporisations. Les interruptions permettent au microcontrôleur de faire d'autres choses en attendant un évènement.

#### Optimisation de la compilation

En pratique, on ne compile pas avec une seule ligne de commande. Quand on a deux fichiers à compiler, ça ne pose pas vraiment de problème. Mais quand on travaille sur un gros projet, les temps de compilation peuvent devenir très longs si on a beaucoup de fichiers. Pour résoudre ce problème, on utilise des fichiers intermédiaires (Extension `.o`). Chaque fichier `.c` est compilé en `.o` et une fois que tous les fichiers `.c` sont compilés, on utilise un linker qui va générer le programme à partir des fichiers `.o` déjà compilé.

Comme chaque fichier `.c` est compilé séparément, la première compilation du projet sera plus longue. Mais une fois que tous les fichiers seront compilés, seuls les fichiers ayant subit des changements devront être recompilés la prochaine fois. Donc, quand le projet est gros, il n'y aura pas beaucoup de fichier à recompiler, seul le linker devra toujours être exécuté.

Pour générer des fichiers `.o`, on doit ajouter le flag `-c` à la compilation. Une fois que tous les fichiers `.o` ont été généré, on peut linker ces fichiers avec la même commande que pour compiler directement les fichiers `.c`. Les étapes de compilations sont donc :
```bash
xc16-gcc -c main.c -Wall -Werror -I. -mcpu=33FJ128MC804 -o main.o
xc16-gcc -c init.c -Wall -Werror -I. -mcpu=33FJ128MC804 -o init.o
xc16-gcc main.o init.o -Wall -Werror -I. -mcpu=33FJ128MC804 -o build.elf -Wl,--defsym=__MPLAB_BUILD=1,--script=p33FJ128MC804.gld,--heap=512,--stack=512,--check-sections,--data-init,--pack-data,--handles,--isr,--no-gc-sections,--fill-upper=0,--stackguard=16,--no-force-link,--smart-io
```

#### Génération du binaire

Une fois qu'on a terminé la compilation, on doit générer le fichier binaire pour qu'il puisse être uploadé sur le microcontrôleur.
```bash
xc16-bin2hex build.elf -omf=elf -o build.hex
```

## Makefile

Pour automatiser la compilation, on utilise un Makefile. Le Makefile contient des scripts qui vont permettre de compiler le code tout seul. Il suffit donc d'écrire le script de compilation une seuls fois pour pouvoir compiler la totalité du projet (ou pour que MPLAB X lance la compilation). En pratique, il suffira donc de lancer le Makefile pour compiler.
```bash
make build
```
Ressource pour apprendre à écrire un Makefile : [Introduction à Makefile](https://gl.developpez.com/tutoriel/outil/makefile/)

## Protocol Buffer pour le système embarqué

Protocol Buffer est un outil multi-plateformes pour sérialiser des structures de données. On l'utilise surtout pour la communication entre différentes cartes électroniques pour pouvoir échanger des données simplement.

[Protocol Buffer](https://developers.google.com/protocol-buffers/docs/overview)

On utilise Nanopd pour pouvoir générer du code source pour les systèmes embarqués. En effet, le langage C n'est pas supporté par Protocol Buffer, mais le système de plugin permet de supporter d'autres langages.

[Nanopb](https://github.com/nanopb/nanopb)

## Docker

Un docker est un environnement virtuel. Les Dockers permettent de déployer facilement un environnement défini (dans notre cas avec tous les outils qui permettent de compiler avec XC16). Le Dockerfile contient donc toutes les commandes nécéssaires à l'installation des outils.

### Dockerfile

Quels points importants sur la création de l'image
* XC16 et son programme d'installation à besoin de bibliothèque 32 bits pour fonctionner, c'est pour cette raison que les paquets `lib32z1`, `libdb1-compat` et `lib32stdc++6` sont installés
* Les programmes d'installation de Microchip se lancent graphiquement par défaut et les étapes doivent être validées au clavier, c'est pour ça que la commande d'installation utilise un pipe pour envoyer des caractères sur l'entrée standard
```bash
echo "\n\n\n\n\n\n\n\n\n\n\n\n\n\ny\n1\n/opt/microchip/xc16\nN\n\nn\nn\nn\n\n\n" | ./mplabxc16linux --mode text
```
* Pour que le Makefile fonctionne correctement, on a aussi besoin d'exporter une variable d'environnement pour utiliser les commandes sans connaître l'emplacement des programmes
```bash
PATH="/opt/microchip/xc16/bin:/opt/microchip/xc16/bin/bin:/opt/nanopb/generator-bin:${PATH}"
```

### Build

Pour utiliser l'image, on doit d'abord build l'image du Docker avec la commande comme n'importe quelle image.
```bash
docker build -t compilation-xc16 docker
```
L'image est compilée à partir du `Dockerfile` dans le répertoire `docker` avec le nom `compilation-xc16`

### Compilation

On peut ensuite lancer la compilation. On lance donc l'image qu'on a appelé `compilation-xc16` avec un volume. Le volume permet à l'image de monter un dossier en dehors de l'image, dans notre cas le projet à compiler. Le volume est ajouté avec l'argument `-v /absolute/path/project.X:/project` qui monte le dossier `/absolute/path/project.X` à l'emplacement `/project` dans le Docker. L'argument `-w /project` permet de se placer dans le répertoire `/project` au démarrage du Docker. L'argument `-it` est optionnel et permet de lancer le Docker en mode interactif.
```bash
docker run -v /absolute/path/project.X:/project -w /project -it compilation-xc16
```